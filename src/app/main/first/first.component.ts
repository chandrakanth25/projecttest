import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as _ from 'underscore';
import { PagerService } from './../../_services/index';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  rowVal = [3, 5, 10, 15, 20];
  testForm: FormGroup;
  currRow: any = 3;
  constructor(private http: HttpClient, private pagerService: PagerService) {
    this.testForm = new FormGroup({
      val: new FormControl()
    });
  }
  private allItems: any;
  pager: any = {};
  pagedItems: any[];
  ngOnInit() {
    this.http.get('./sample_data.json')
      .subscribe(data => {
        this.allItems = data;
        this.setPage(1);
      });
      this.testForm.controls['val'].setValue(this.currRow, { onlySelf: true });
  }
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page, this.currRow);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  SelectedValue(e) {
    this.currRow = parseInt(e.currentTarget.value.substring(3), 10);
    this.setPage(1);
  }
  setClickedRow(e) {
    alert('You have clicked ID: ' + e.id + ' Row Status is: ' + e.status);
  }
}
